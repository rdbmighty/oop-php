<?php

    class Animal{
        public $name;
        public $legs = 4;
        public $cold_blooded;

        public function __construct($name,$cold_blooded){
            $this->name = $name;
            $this->cold_blooded = $cold_blooded;
        }
        
    }

?>